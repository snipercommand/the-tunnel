﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	float m_progress = 0;

	TunnelPiece m_currentTunnelPiece 	= null;

	[SerializeField]
	TunnelPiece m_StartPiece			= null;

	TunnelPiece.ELaneType m_currentLane = TunnelPiece.ELaneType.Left;


	Vector3 m_targetCameraLocalPos		= Vector3.zero;

	[SerializeField]
	Vector3[] m_LaneCameraLocalPositions;

	[SerializeField]
	Transform cameraTransform = null;

	// Use this for initialization
	void Start () 
	{
		Reset ();
	}

	// Update is called once per frame
	void Update () 
	{
	
		m_progress += Time.deltaTime;

		Vector3 OldPlayerPos = transform.position;
		Vector3 NewTargetPlayerPos = transform.position;

		if( m_currentTunnelPiece.TryGetPlayerPosition( m_currentLane, m_progress, out NewTargetPlayerPos ) )
		{
			transform.position = Vector3.Slerp(OldPlayerPos, NewTargetPlayerPos,0.2f);
		}
		else
		{
			m_progress = 0;
			m_currentTunnelPiece = m_currentTunnelPiece.GetNextTunnelPiece();
		}

		cameraTransform.localPosition = Vector3.Slerp (cameraTransform.localPosition, m_targetCameraLocalPos, 0.2f);

		if( Input.GetKeyUp(KeyCode.R ) )
		{
			Reset ();
		}

		if (Input.GetKeyDown (KeyCode.Q)) 
		{
			SwitchLane(TunnelPiece.ELaneType.Left);
		}
		if (Input.GetKeyDown (KeyCode.D)) 
		{
			SwitchLane(TunnelPiece.ELaneType.Right);
		}
		if (Input.GetKeyDown (KeyCode.Z)) 
		{
			SwitchLane(TunnelPiece.ELaneType.Up);
		}


	}

	void SwitchLane(TunnelPiece.ELaneType newLane)
	{
		m_currentLane = newLane;

		m_targetCameraLocalPos = m_LaneCameraLocalPositions [(int)newLane];
	}

	
	void Reset()
	{
		m_progress = 0;
		m_currentTunnelPiece = m_StartPiece;
		SwitchLane (TunnelPiece.ELaneType.Left);
		//Imediately move camera to target pos
		cameraTransform.localPosition = m_targetCameraLocalPos;
	}

}
