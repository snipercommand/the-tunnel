﻿Shader "Q/Q_FullScreen_Test" 
{
	Properties 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Pass
		{	
		//ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
		
CGPROGRAM
	#pragma vertex vert_img
	#pragma fragment frag
	#include "UnityCG.cginc"


	uniform sampler2D _MainTex;
	uniform float4	 lum;
	uniform float4 colorToReplace;
	uniform float4 replacementColor;

//	float4 frag( v2f_img i ) : COLOR
//	{
//
//		float4 pix = tex2D(_MainTex, i.uv);
//
//		//obtain luminence value
//		if( pix.b > colorToReplace.b )
//		{
//			pix = lerp(0,replacementColor,pix.b);
// 		}
//
//
//		float dist = distance(i.uv, float2(0.5,0.5));
//		pix *= smoothstep(0.50 ,0.35 ,dist);
//
//		return pix;
//	}


		struct appdata 
		{
			float4 vertexPos : POSITION;
          	float3 normal : TEXCOORD0;  
		};

		struct v2f 
		{
			float4 pos : SV_POSITION;
			float3 normal : TEXCOORD0;
			
		};

	
	
		v2f vert(appdata IN)
		{
			v2f OUT;
			OUT.pos = mul( UNITY_MATRIX_MVP, IN.vertexPos );
			OUT.normal = mul((float3x3)_Object2World, IN.normal);
                    
			return OUT;
		}

		fixed4 frag (v2f IN) : COLOR
		{
			return float4(IN.normal,1);	
		}


ENDCG
	}
} 

	FallBack "Diffuse"
}
