using UnityEngine;
using System.Collections;

public class TunnelPiece : MonoBehaviour 
{
	[SerializeField]
	Transform LeftLaneStartTransf;

	[SerializeField]
	Transform LeftLaneEndTransf;

	[SerializeField]
	Transform RightLaneStartTransf;

	[SerializeField]
	Transform RightLaneEndTransf;


	
	[SerializeField]
	Transform UpLaneStartTransf;
	
	[SerializeField]
	Transform UpLaneEndTransf;


	public enum ELaneType
	{
		Left,
		Right,
		Up
	}

	[SerializeField]
	ELaneType m_laneType = ELaneType.Left;

	[SerializeField]
	int m_amountOfPickupsOnLane = 3;

	[SerializeField]
	GameObject m_pickupPrefab = null;

	[SerializeField]
	TunnelPiece m_nextTunnelPiece;

	// Use this for initialization
	void Start () 
	{
	
		PlacePickupsAlongLane (m_laneType);

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void PlacePickupsAlongLane(ELaneType laneType)
	{

		Transform ChosenLaneStartTransform = LeftLaneStartTransf;
		Transform ChosenLaneEndTransform = LeftLaneEndTransf;

		switch( laneType )
		{
		case ELaneType.Left:
			ChosenLaneStartTransform 	= LeftLaneStartTransf;
			ChosenLaneEndTransform 		= LeftLaneEndTransf;
			break;
	
		case ELaneType.Right:
			ChosenLaneStartTransform 	= RightLaneStartTransf;
			ChosenLaneEndTransform 		= RightLaneEndTransf;
			break;
	
		case ELaneType.Up:
			ChosenLaneStartTransform 	= UpLaneStartTransf;
			ChosenLaneEndTransform 		= UpLaneEndTransf;
			break;

		}

		float distanceBetweenLanePoints = (ChosenLaneEndTransform.position - ChosenLaneStartTransform.position).magnitude / m_amountOfPickupsOnLane;
		Vector3 laneDirection = (ChosenLaneEndTransform.position - ChosenLaneStartTransform.position).normalized;

		for( int i = 0; i < m_amountOfPickupsOnLane; i++ )
		{
			GameObject.Instantiate( m_pickupPrefab, ( laneDirection * (distanceBetweenLanePoints/2)) + ChosenLaneStartTransform.position + ( ( laneDirection * distanceBetweenLanePoints  ) * i ), Quaternion.identity );
		}
	}

	//------------------------------------------------------------
	// Returns true if position is still within This TunnelPiece
	// Otherwise False
	//-------------------------------------------------------------
	public bool TryGetPlayerPosition(ELaneType laneType, float progress, out Vector3 playerPosition )
    {
		if( progress <= 1 )
		{
			Transform ChosenLaneStartTransform = LeftLaneStartTransf;
			Transform ChosenLaneEndTransform = LeftLaneEndTransf;
			
			switch( laneType )
			{
			case ELaneType.Left:
				ChosenLaneStartTransform 	= LeftLaneStartTransf;
				ChosenLaneEndTransform 		= LeftLaneEndTransf;
				break;
				
			case ELaneType.Right:
				ChosenLaneStartTransform 	= RightLaneStartTransf;
				ChosenLaneEndTransform 		= RightLaneEndTransf;
				break;
				
			case ELaneType.Up:
				ChosenLaneStartTransform 	= UpLaneStartTransf;
				ChosenLaneEndTransform 		= UpLaneEndTransf;
				break;
				
			}

			playerPosition = ChosenLaneStartTransform.position + ( (ChosenLaneEndTransform.position - ChosenLaneStartTransform.position) * progress);
			return true;
		}
		else
		{
			playerPosition = Vector3.zero;
			return false;
		}
	}


	public TunnelPiece GetNextTunnelPiece()
	{
		return m_nextTunnelPiece;
	}
}
