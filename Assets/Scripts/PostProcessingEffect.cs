﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PostProcessingEffect : MonoBehaviour {

	/*
	[SerializeField]
	RenderTexture m_renderTex;

	Camera 	m_cam;

	[SerializeField]
	Texture	m_blitTex;

	[SerializeField]
	Material m_blitMat;

	// Use this for initialization
	void Start () 
	{
		m_renderTex.Create();

		m_cam = this.GetComponent<Camera> ();
	}

	void OnPreRender()
	{
		m_cam.targetTexture = m_renderTex;
	}

	void OnPostRender()
	{
		Graphics.Blit (m_blitTex, m_renderTex, m_blitMat);
		m_cam.targetTexture = null;

		Graphics.Blit (m_renderTex, null as RenderTexture);
		
	}
	*/

	public Color luminence;

	public Color colorToReplace;
	public Color replacementColor;
	

	[SerializeField]
	Shader shader;

	[SerializeField]
	Material mat;

	void Start()
	{
	}

	void OnRenderImage( RenderTexture source, RenderTexture destination)
	{
		mat.SetVector( "lum", new Vector4( luminence.g, luminence.g, luminence.g, luminence.g) );
		mat.SetVector( "colorToReplace", new Vector4( colorToReplace.r, colorToReplace.g, colorToReplace.b, colorToReplace.a) );
		mat.SetVector( "replacementColor", new Vector4( replacementColor.r, replacementColor.g, replacementColor.b, replacementColor.a) );

		Graphics.Blit( source, destination, mat);
	}

}
