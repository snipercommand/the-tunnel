﻿Shader ".Q/Q_Curved_Unlit" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Curvature ("Curvature", Float) = 0.001
	}
	SubShader {
		
		Pass
		{
		CGPROGRAM
		#pragma vertex vert 
		#pragma fragment frag 
		
		
		struct appdata 
		{
			float4 vertexPos : POSITION;
		};

		struct v2f 
		{
			float4 pos : SV_POSITION;
		};


		fixed4 _Color;
		uniform float  _Curvature;

		v2f vert(appdata IN)
		{
			v2f OUT;
			
			OUT.pos = mul( UNITY_MATRIX_MVP, IN.vertexPos );
			return OUT;
		}

		fixed4 frag (v2f IN) : COLOR
		{
		
			return _Color;	
		}
		ENDCG
		}
	} 
	FallBack "Diffuse"
}
